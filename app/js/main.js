// Require JS  Config File
require({
			baseUrl: 'js/',
			paths: {
				'ua-parser': '../lib/ua-parser-js/src/ua-parser'
				, 'angular': '../lib/angular/index'
				, 'angular-resource': '../lib/angular-resource/index'
				, 'angular-route': '../lib/angular-route/index'
			},
			shim: {
				'app': {
					'deps': [
						  'angular'
						, 'angular-route'
						, 'angular-resource'
					]
				},
				'angular-resource': { 'deps': ['angular']},
				'angular-route': { 'deps': ['angular'] },
				'routes': { 'deps': [
					'app'
				]},
				'ApplicationController': {
					'deps': [
						'app'
					]}
			}
		},
		[
			'require'
			, 'routes'
			, 'ApplicationController'

		],
		function (require) {
			return require(
				[
					'bootstrap'
				]
			)
		}
);