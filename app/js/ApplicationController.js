/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController'
	,[
		'$rootScope'
		, '$http'
		, '$scope'
		, '$resource'
		, function
			(
				$rootScope
				, $http
				, $scope
				, $resource
				) {


			$scope.message = "Hello"

			$scope.publish = function(){
				postRequest({request:'fromAngular'});

			}


			function postRequest(data){
				$http.post('http://localhost/angular-js-php-restful/api/',data).success(function(success) {
					console.log('success',success)
					$scope.message = JSON.stringify(success)
				})
			}


		}]
)