<?php


/**
 * FOR DEVELOPMENT ONLY:
 * 1. Make sure you have the mod_headers apache module installed
 * /etc/apache2/mods-enabled
 * If you dont have it install:
 * sudo ln -s /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/headers.load
 *
 * 2. Add the Access-Control-Allow-Origin header to all HTTP response,
 * add the following to the desired <Directory>
 * Header set Access-Control-Allow-Origin "*"
 *
 * Within this file only add the headers "Access-Control-Allow-Headers: Content-Type"
 * remove this headers once you have it set onto your production server, and both client and server reside on the same machine
 */
header('Access-Control-Allow-Headers: Content-Type');


//some kind of return data
$data = array("FROM" => "PHP RESTful:api");

/*
 * http://php.net/manual/en/function.http-get-request-body.php
 * Get the raw request body (e.g. POST or PUT data).
 * http_get_request_body needs to have PECL_HTTP module installed
 */
//$post_body = http_get_request_body ();


//get the data
$post_body = file_get_contents('php://input');

//Do what ever you want with the data


$json = json_encode($data);
echo $json;